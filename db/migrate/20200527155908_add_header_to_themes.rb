class AddHeaderToThemes < ActiveRecord::Migration[5.2]
  def change
    add_reference :themes, :header, foreign_key: true
  end
end
