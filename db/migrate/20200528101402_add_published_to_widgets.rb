class AddPublishedToWidgets < ActiveRecord::Migration[5.2]
  def change
    add_column :widgets, :published, :boolean
  end
end
