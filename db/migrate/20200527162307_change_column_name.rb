class ChangeColumnName < ActiveRecord::Migration[5.2]
  def change
    rename_column :menus, :type, :field_type
  end
end
