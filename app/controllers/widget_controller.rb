class WidgetController < ApplicationController
  def show
    @widget = Widget.find(params[:id])
    @cms = Theme.find(params[:id])
    @header = Header.find(@widget.header_id)
    @menus = @header.menus
    @logo = @header.logo
  end

  def update
    puts "params <<<<<< #{params}"
    Widget.all.each do |w|
      if w.id == params[:id].to_i
        w.published = true
      else
        w.published = false
      end
      w.save
    end
    redirect_to spree_path
  end
end
