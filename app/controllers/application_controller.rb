class ApplicationController < ActionController::Base
    before_action :widget

    private
    def widget
        puts "<<<<<<<<<<<<<<<< Test <<<<<<<<<<<<<<"
        if (@widget = Widget.find_by_published(true))
            puts "@widget <<<<<<<<<<<<<<< #{@widget}"
            # @widget = nil
            @header = Header.find(@widget.header_id)
            @menus = @header.menus
            @logo = @header.logo
        end
    end
end
