class Header < ApplicationRecord
    has_one_attached :logo
    has_many :menus, dependent: :destroy
    accepts_nested_attributes_for :menus, allow_destroy: true, reject_if: proc { |att| att['name'].blank? }
    has_one :widget
    has_one :theme
end
