module ApplicationHelper
  def header(themeid)
    if themeid == 2
      "<header class='main_menu home_menu'><div class='container-fluid'><div class='row align-items-center justify-content-center'><div class='col-lg-11'><nav class='navbar navbar-expand-lg navbar-light'><a class='navbar-brand' href='index.html'><img src='/assets/winter_img/logo.png'/></a><button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent' aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'></button><div class='collapse navbar-collapse main-menu-item' id='navbarSupportedContent'><ul class='navbar-nav'>" + "#{@menus.map { |menu| menu['field_type'] == 'text' ? "<li class='nav-item'><a class='nav-link' href=#{menu['redirect_url']}>#{menu['label']}</a></li>" : "<li class='nav-item dropdown'><a class='nav-link dropdown-toggle' href='blog.html' id='navbarDropdown_1' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>#{menu['label']}</a><div class='dropdown-menu' aria-labelledby='navbarDropdown_1'>" + "#{menu.values.split(";").map { |value| "<a class='dropdown-item' href=#{value.split(',').last}>#{value.split(',').first}</a>" }.join('')}" + '</div></li>'}.join('')}" + "</ul></div></nav></div></div></div></header>"
    elsif themeid == 3
      "<nav class='navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light' id='ftco-navbar'><div class='container'><a class='navbar-brand' href='index.html'><img src='/assets/winter_img/logo.png'/></a><div class='collapse navbar-collapse' id='ftco-nav'><ul class='navbar-nav ml-auto'>" + "#{@menus.map { |menu| menu['field_type'] == 'text' ? "<li class='nav-item active'><a class='nav-link' href=#{menu['redirect_url']}>#{menu['label']}</a></li>" : "<li class='nav-item dropdown'><a class='nav-link dropdown-toggle' href='blog.html' id='dropdown04' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>#{menu['label']}</a><div class='dropdown-menu' aria-labelledby='dropdown04'>" + "#{menu.values.split(";").map { |value| "<a class='dropdown-item' href=#{value.split(',').last}>#{value.split(',').first}</a>" }.join('')}" + '</div></li>'}.join('')}" + "</ul></div></div></nav>"
    else
      "<header class='header-section'><nav class='main-navbar'><div class='container'><a class='navbar-brand' href='index.html'><img src='/assets/winter_img/logo.png'/><ul class='main-menu'>" + "#{@menus.map { |menu| menu['field_type'] == 'text' ? "<li><a href=#{menu['redirect_url']}>#{menu['label']}</a></li>" : "<li><a href='index.html'>#{menu['label']}</a><ul class='sub-menu'>" + "#{menu.values.split(";").map { |value| "<li><a href=#{value.split(',').last}>#{value.split(',').first}</a></li>" }.join('')}" + '</ul></li>'}.join('')}" + "</div></nav></header>"
    end 
  end

  def link_to_add_row(name, f, association, **args)
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.simple_fields_for(association, new_object, child_index: id) do |builder|
      render(association.to_s.singularize, f: builder)
    end
    link_to(name, '#', class: "add_fields " + args[:class], data: {id: id, fields: fields.gsub("\n", "")})
  end
end
