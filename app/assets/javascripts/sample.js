
$(document).ready(function() {
    $(".mediumcontainer").draggable({ helper: 'clone'});
    $(".smallcontainer").draggable({ helper: 'clone'});
    $('.largecontainer').droppable({
        accept: '.mediumcontainer',
        drop: function(ev, ui) {
            var item = $(ui.draggable).clone();
            $(this).append(item);

            $('.mediumcontainer').droppable({
                accept: '.smallcontainer',
                drop: function(ev, ui) {
                    var droppeditem = $(ui.draggable).clone();
                    $(this).append(droppeditem);
                }
            })
        }

        
    })
    
})