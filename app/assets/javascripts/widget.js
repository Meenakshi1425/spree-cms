$(document).on('turbolinks:load', function() {
    
    $(".blockwidget").draggable({ helper: 'clone', cursor: "move", cancel: '#editor'});
    $(".blockelement").draggable({ helper: 'clone', cursor: "move", cancel: '#editor'});

    $('#cartwidget').droppable({
        accept: '.blockwidget',
        activeClass: 'active',
        hoverClass: 'hover',
        drop: function(ev, ui) {
            var droppeditem = $(ui.draggable).clone();
            droppeditem.find('.img-element').remove();
            droppeditem.find('.d-none').removeClass('d-none');
            droppeditem.find('.row').append($('<button type="button" class="btn btn-default btn-xs remove"><span class="glyphicon glyphicon-trash"></span></button>'));
            droppeditem.find('.remove').click(function () { 
                console.log("delete");
                $(this).parent().parent().detach() 
            });
            $(this).append(droppeditem);

            console.log("widget");

            $('.element').droppable({
                accept: '.blockelement',
                drop: function(ev, ui) {
                    console.log("element");
                    console.log('test', $(this));
                    var item = $(ui.draggable).clone();
                    item.find('.img-element').remove();
                    item.find('.d-none').removeClass('d-none');
                    droppeditem.find('.split').toggleClass('split element-split');
                    droppeditem.find('.my-container-left').toggleClass('my-container-left element-my-container-left');
                    console.log("item14", item);
                    item.find('.content').append($('<button type="button" class="btn btn-default btn-xs remove-element"><span class="glyphicon glyphicon-trash"></span></button>'));
                    $('.remove-element').click(function () {
                        console.log("delete111"); 
                        $(this).parent().detach() 
                    });
                    $(this).append(item);
                }
            })
        }
    }).sortable({
        items: '.element-split',
        sort: function() {
          $( this ).removeClass( "active" );
        }
    });
    

    $('.save-btn').click(function() {
        console.log("this", $(this).find('#cartwidget'));
    })
})
